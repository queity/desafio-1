package com.qty.desafio.infrastructure.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qty.desafio.domain.model.TarefaItem;
import com.qty.desafio.domain.repository.TarefaItemRepository;

@Component
public class TarefaItemRepositoryImpl implements TarefaItemRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<TarefaItem> listar() {
		return entityManager.createQuery("from TarefaItem", TarefaItem.class).getResultList();
	}
	
	@Override
	public TarefaItem buscar(Integer id) {
		return entityManager.find(TarefaItem.class, id);
	}
	
	@Transactional
	@Override
	public TarefaItem salvar(TarefaItem tarefaItem) {
		return entityManager.merge(tarefaItem);
	}
	
	@Transactional
	@Override
	public void remover(Integer id) {
		TarefaItem tarefaItem = buscar(id);
		if (tarefaItem == null) {
			throw new EmptyResultDataAccessException(1);
		}
		entityManager.remove(tarefaItem);
	}
	
}
