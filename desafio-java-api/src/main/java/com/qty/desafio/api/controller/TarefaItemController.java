package com.qty.desafio.api.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.qty.desafio.domain.exception.EntidadeEmUsoException;
import com.qty.desafio.domain.exception.EntidadeNaoEncontradaException;
import com.qty.desafio.domain.model.Tarefa;
import com.qty.desafio.domain.model.TarefaItem;
import com.qty.desafio.domain.repository.TarefaItemRepository;
import com.qty.desafio.domain.service.CadastroTarefaItemService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/itens")
public class TarefaItemController {

	@Autowired
	private TarefaItemRepository tarefaItemRepository;
	
	@Autowired
	private CadastroTarefaItemService cadastroTarefaItemService;
	
	@GetMapping
	public List<TarefaItem> listar() {
		return tarefaItemRepository.listar();
	}
	
	@GetMapping("/{idItem}")
	public ResponseEntity<TarefaItem> buscar(@PathVariable Integer idItem) {
		TarefaItem tarefaItem = tarefaItemRepository.buscar(idItem);
		
		if (tarefaItem != null) {
			return ResponseEntity.ok(tarefaItem);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public TarefaItem adicionar(@RequestBody TarefaItem tarefaItem) {
		return cadastroTarefaItemService.salvar(tarefaItem);
	}
	
	@PutMapping("/{idItem}")
	public ResponseEntity<TarefaItem> atualizar(@PathVariable Integer idItem, @RequestBody TarefaItem tarefaItem) {
		TarefaItem tarefaItemAtual = tarefaItemRepository.buscar(idItem);
		
		if (tarefaItemAtual != null) {
			BeanUtils.copyProperties(tarefaItem, tarefaItemAtual, "id");
			tarefaItemAtual = cadastroTarefaItemService.salvar(tarefaItemAtual);
			return ResponseEntity.ok(tarefaItemAtual);
		}
		
		
		return ResponseEntity.notFound().build();
	}

	@DeleteMapping("/{idItem}")
	public ResponseEntity<Tarefa> remover(@PathVariable Integer idItem) {
		try {
			cadastroTarefaItemService.excluir(idItem);
			return ResponseEntity.noContent().build();
		} catch (EntidadeNaoEncontradaException e) {
			return ResponseEntity.notFound().build();
		} catch (EntidadeEmUsoException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
	}
	
}
