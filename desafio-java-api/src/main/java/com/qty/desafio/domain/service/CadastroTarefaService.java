package com.qty.desafio.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.qty.desafio.domain.exception.EntidadeEmUsoException;
import com.qty.desafio.domain.exception.EntidadeNaoEncontradaException;
import com.qty.desafio.domain.model.Tarefa;
import com.qty.desafio.domain.repository.TarefaRepository;

@Service
public class CadastroTarefaService {

	@Autowired
	private TarefaRepository tarefaRepository;
	
	public Tarefa salvar(Tarefa tarefa) {
		return tarefaRepository.salvar(tarefa);
	}
	
	public void excluir(Integer idTarefa) {
		try {
			tarefaRepository.remover(idTarefa);
		} catch (EmptyResultDataAccessException e) {
			throw new EntidadeNaoEncontradaException (
				String.format("Não existe um cadastro de tarefa com código %d", idTarefa));
		} catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException (
				String.format("Tarefa de código %d não pode ser removida, pois está em uso", idTarefa));
		}
	}
	
}
