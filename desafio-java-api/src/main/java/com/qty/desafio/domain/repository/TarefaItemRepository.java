package com.qty.desafio.domain.repository;

import java.util.List;

import com.qty.desafio.domain.model.TarefaItem;

public interface TarefaItemRepository {

	List<TarefaItem> listar();
	TarefaItem buscar(Integer id);
	TarefaItem salvar(TarefaItem tarefaItem);
	void remover(Integer id);
	
}
