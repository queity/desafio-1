import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TarefaItemService } from '../../services/tarefa.item.service';
import { TarefaItem } from '../../services/model/tarefa.item';

@Component({
    selector: 'app-delete-tarefa-item',
    templateUrl: './tarefa-item-delete.component.html',
    styleUrls: ['./tarefa-item-delete.component.css']
})
export class TarefaItemDeleteComponent {

    id: number;

    constructor(private route: ActivatedRoute, private router: Router, private tarefaItemService: TarefaItemService) {

        this.route.params.subscribe(parametro => {
            if (parametro['idItem'] != undefined) {
                this.id = Number(parametro['idItem']);
                this.tarefaItemService.remover(this.id).subscribe(result => this.gotoTarefaList());
            }
        })

    }

    gotoTarefaList() {
        this.router.navigate(['/consulta-tarefa-item']);
    }

}