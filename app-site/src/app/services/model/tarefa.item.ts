import { Tarefa } from './tarefa';

export class TarefaItem {
    id: number;
    descricao: string;
    tarefa: Tarefa;
}