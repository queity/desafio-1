import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TarefaService } from '../../services/tarefa.service';
import { Tarefa } from '../../services/model/tarefa';

@Component({
    selector: 'app-cadastro-tarefa',
    templateUrl: './tarefa-cadastro.component.html',
    styleUrls: ['./tarefa-cadastro.component.css']
})
export class TarefaCadastroComponent {

    tarefa: Tarefa;

    constructor(private route: ActivatedRoute, private router: Router, private tarefaService: TarefaService) {
        this.tarefa = new Tarefa();
    }

    onSubmit() {
        this.tarefaService.adicionar(this.tarefa).subscribe(result => this.gotoTarefaList());
    }

    gotoTarefaList() {
        this.router.navigate(['/consulta-tarefa']);
    }

    adicionarItemTarefa(idTarefa:number):void{
        this.router.navigate(['/cadastro-tarefa-item',idTarefa]);
    }

}