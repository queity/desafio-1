import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TarefaService } from '../../services/tarefa.service';
import { Tarefa } from '../../services/model/tarefa';

@Component({
    selector: 'app-consulta-tarefa',
    templateUrl: './tarefa-consulta.component.html',
    styleUrls: ["./tarefa-consulta.component.css"]
})
export class TarefaConsultaComponent implements OnInit {

    tarefas: Tarefa[];

    constructor(private route: ActivatedRoute, private router: Router, private tarefaService: TarefaService) {
    }

    ngOnInit() {
        this.tarefaService.listar().subscribe(data => {
            this.tarefas = data;
        });
    }

    adicionarItemTarefa(idTarefa:number):void{
        this.router.navigate(['/cadastro-tarefa-item',idTarefa]);
    }

}